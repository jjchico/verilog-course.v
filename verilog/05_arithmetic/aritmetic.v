// Design:      arithmetic
// File:        arithmetic.v
// Description: Arithmetic circuits examples in Verilog
// Author:      Jorge Juan-Chico <jjchico@gmail.com>
// Date:        10/12/2009 (initial version)

/*
   Unit 5: Arithmetic circuits

   In this unit we will see three examples of arithmetic circuits design:

   Lesson 5.1 (adders.v): description of adder circuits at various levels
   and comparison of results.

   Lesson 5.2 (addsub.v): two alternative versions of an adder/subtractor of
   number represented in two's complement.

   Lesson 5.3 (alu.v): design of an Arithmetic and Logic Unit (ALU) that is
   able to perform various simple calculations.

   Also in this unit we introduce some additional concepts of the Verilog HDL
   like the arithmetic operators and the 'generate' directive. We also do
   additional training in making structural descriptions and test benches.
*/

module arithmetic();

    initial
        $display("Arithmetic circuits.");

endmodule
