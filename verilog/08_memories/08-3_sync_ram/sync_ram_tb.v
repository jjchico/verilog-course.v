// Design:      sync_ram
// File:        sync_ram_tb.v
// Description: Synchronous RAM design. Test bench.
// Author:      Jorge Juan-Chico <jjchico@gmail.com>
// Date:        15/01/2023 (initial version)

/*
   Lesson 8.3. Synchronous RAM design.

   This file contains a test bench to test the RAM modules in file
   'sync_ram.v'.
 */

// This test bench simulates memories ram32x8_rf and ram32x8_wf in parallel
// in order to compared their behavior. Each memory is initialized with a
// different content loaded from files mem1.list and mem2.list. The testing
// procedure is as follows:
//
//   * The contents of both memories is read testing their operation in
//     read mode.
//   * Random numbers are written in both memories to test the operation in
//     write mode. It should be seen that, after the active clock edge,
//     ram32x8_rf outputs the previous content of the written cell while
//     ram32x8_wf outputs the new written data.
//   * The complete contents of both memories is read again to test the
//     previous writing operations. Both memories should output the same
//     data.

`timescale 1ns / 1ps

`ifndef TCYCLE
    `define TCYCLE 20  // clock period
`endif

`ifndef SEED
    `define SEED 1    // initial seed for the random number generator
`endif

// Test bench

module test ();

    reg clk;            // clock signal
    wire [4:0] a;       // address lines
    reg [7:0] din;      // input data lines
    wire we;            // write enable
    wire [7:0] dout1;   // data output (read-first)
    wire [7:0] dout2;   // data output (write-first)

    reg [5:0] cont;         // auxiliary variable to assign 'a' and 'we'
    integer seed = `SEED;   // auxiliary variable (seed)

    // Circuits under test
    ram32x8_rf ram1(.clk(clk), .a(a), .din(din), .we(we), .dout(dout1));
    ram32x8_wf ram2(.clk(clk), .a(a), .din(din), .we(we), .dout(dout2));

    // Address and write-enable generation
    /* Makes it easier to go through all addresses with we=0 and we=1 */
    assign a = cont[4:0];
    assign we = cont[5];

    // Testing process
    initial begin
        // Waveform generation
        $dumpfile("sync_ram_tb.vcd");
        $dumpvars(0, test);

        // Initial memory data loading
        /* Memories (or any variable) may also be initialized from a
         * higher-level module like a test bench. See how the path in the
         * design's hierarchy to the variable is expressed:
         * <instance_name>.<variable> */
        $readmemb("mem1.list", ram1.mem);
        $readmemb("mem2.list", ram2.mem);

        // Results list header
        $display("we | a din | dout1 dout2");

        // Simulation control signals initial values
        clk = 1'b0;
        cont = 0;               // starts with we=0 and a=0
        din = $random(seed);    // initial random data in 'din'

        // Simulation end control
        /* The address space is traversed three times: read, write, read */
        #(3*32*`TCYCLE) $finish;

    end

    // Clock signal generator
    always
        #(`TCYCLE/2) clk = ~clk;

    // Display signals after each active clock edge
    always @(posedge clk)
        #1      // makes sure signals are already stable
        $display(" %b | %h %h | %h %h", we, a, din, dout1, dout2);

    // New inputs are generated after the active clock edge
    always @(posedge clk) begin
        #2      // let previous data to be displayed
        cont = cont + 1;
        din = $random(seed);
    end

endmodule // test

/*
   EXERCISES

   2. Compile and simulate the test bench with:

        $ iverilog sync_ram.v sync_ram_tb.v
        $ vvp a.out

      Check the test output on the terminal. See how the contents of both
      memories is displayed in the first lines while we=0 (outputs 'dout1' and
      'dout2'). When 'we' changes to 1 the writing o both memories takes place.
      See how ram32x8_rf generates the previous value in 'dout1' during the
      write operation while ram32x8_wf generates the new stored value in
      'dout2'. In the last lines displayed with we=0 both memories are read
      again and they should display the same contents.

   3. What the waveforms generated by the simulation with:

        $ gtkwave sync_ram_tb.vcd

      Add signals 'clk', 'we', 'din', 'dout1' and 'dout2' to the display area
      and see when each signal changes and how it relates to the operation
      mode of each memory module.

   4. A typical behavior of synchronous RAM's is that the output is not
      modified during a writing operation. Modify the description of the
      memory modules to behave like that and test them again using this same
      test bench. Are there any differences in the simulation results in this
      case?
*/
